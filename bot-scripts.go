package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

var bots = make(map[string]string)
var botparentpath = "scripts/bots"

func initBotScripts() {
	err := filepath.WalkDir("./"+botparentpath, inibotscript)
	if err != nil {
		fmt.Printf("error on reading bot directory %w", err)
	}
}

func inibotscript(path string, d os.DirEntry, err error) error {
	if err != nil {
		return err
	}
	if d.IsDir() {
		return nil
	}
	f, err := os.Open("./" + botparentpath + "/" + d.Name())
	body, err := io.ReadAll(f)
	if err != nil {
		return err
	}
	if err != nil {
		fmt.Println("error reading client js")
	}
	bots[cleanbotname(d.Name())] = string(body)
	fmt.Println(d.Name())
	return nil
}

func cleanbotname(s string) string {
	return strings.Replace(s, ".js", "", 1)
}

func getBotByName(n string) string {
	if b, ok := bots[n]; ok {
		return b
	}
	return bots["dumbest"]
}

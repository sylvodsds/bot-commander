package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/fetch"
	cdplog "github.com/chromedp/cdproto/log"
	"github.com/chromedp/chromedp"
	_ "github.com/joho/godotenv/autoload"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	cdpruntime "github.com/chromedp/cdproto/runtime"
)

type Worm struct {
	red       string
	green     string
	blue      string
	name      string
	botscript string
	verbosity string
	follows   string
}

var api string

func main() {
	roomId := os.Getenv("ROOM_ID") // builder
	fmt.Println("room_id " + roomId)
	roomId = strings.Replace(roomId, "https://www.webliero.com/?v=20&c=", "", 1)
	roomId = strings.Replace(roomId, "https://ext-proxy.fly.Dev/?v=20&c=", "", 1)
	initBotScripts()
	initWLScript()

	// create chrome instance
	ctx, cancel := chromedp.NewContext(
		context.Background(),
		//chromedp.WithDebugf(log.Printf),
	)
	defer cancel()
	wg := &sync.WaitGroup{}

	//createBotRoutine(ctx, wg, roomId, Worm{name: "ophi wins if I win", red: "0", green: "0", blue: "180", botscript: "dumb" /*, verbosity: "debug"*/})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "dsds wins if I win", red: "230", green: "1", blue: "230", botscript: "dumb", follows: "dsds💚[ASS]"})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "ole wins if I win", red: "150", green: "1", blue: "150", botscript: "dumb"})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "fw wins if I win", red: "50", green: "250", blue: "50", botscript: "dumb"})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "roo wins if I win", red: "100", green: "100", blue: "50", botscript: "dumb"})
	//	createBotRoutine(ctx, wg, roomId, Worm{name: "pacer wins if I win", red: "230", green: "1", blue: "230", botscript: "dumb" /*, verbosity: "debug"*/})
	//
	//	createBotRoutine(ctx, wg, roomId, Worm{name: "john is a bot too", red: "0", green: "0", blue: "180", botscript: "dumbest"})
	//
	//	createBotRoutine(ctx, wg, roomId, Worm{name: "jane likes johnny", red: "230", green: "1", blue: "230", botscript: "dumb"})

	createBotRoutine(ctx, wg, roomId, Worm{name: "john is a bot", red: "0", green: "0", blue: "180", botscript: "dumb", verbosity: "default"})
	//	createBotRoutine(ctx, wg, roomId, Worm{name: "jane likes johnny", red: "230", green: "1", blue: "230", botscript: "dumbest"})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "kitagawa is a bot", red: "0", green: "20", blue: "120", botscript: "dumb", verbosity: "default"})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "jane too likes johnny too", red: "210", green: "100", blue: "210", botscript: "dumbest"})

	//
	//createBotRoutine(ctx, wg, roomId, Worm{name: "john is a bot", red: "0", green: "0", blue: "180", botscript: "dumbest"})
	//createBotRoutine(ctx, wg, roomId, Worm{name: "jane likes johnny", red: "230", green: "1", blue: "230", botscript: "dumbest"})

	wg.Wait()
}
func createBotRoutine(ctx context.Context, wg *sync.WaitGroup, roomId string, worm Worm) {
	wg.Add(1)
	time.Sleep(3 * time.Second)
	go createBot(ctx, wg, roomId, worm)
}
func createBot(ctx context.Context, wg *sync.WaitGroup, roomId string, worm Worm) {
	defer wg.Done()
	ctx, cancel := chromedp.NewContext(ctx)
	defer cancel()
	var res []byte

	// navigate to a page, wait for an element, click
	var example string
	replaceScript(ctx)

	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *cdpruntime.EventConsoleAPICalled:
			for _, arg := range ev.Args {
				fmt.Printf("%v %s %s - %s\n", time.Now(), worm.name, arg.Type, arg.Value)
			}
		}
	})

	err := chromedp.Run(ctx,
		fetch.Enable(),
		cdplog.Enable(),
		chromedp.Navigate(fmt.Sprintf(`https://ext-proxy.fly.dev/?v=20&c=%s`, roomId)),
		// wait for nickname dialog to be visible

		chromedp.WaitVisible(`body > .view-container .dialog.choose-nickname-view`),
		// set name
		//<input style="width: 100%" data-hook="input" type="text" placeholder="Nickname" maxlength="25">
		chromedp.SetValue(`input[placeholder="Nickname"]`, worm.name),
		//<input class="slider" type="range" data-hook="rslider" min="0" max="255"> gslider bslider
		chromedp.SetValue(`input[data-hook="rslider"]`, worm.red),
		chromedp.SetValue(`input[data-hook="gslider"]`, worm.green),
		chromedp.SetValue(`input[data-hook="bslider"]`, worm.blue),
		//<button class="btn-1 margin-center" sNodeVisibletyle="width: 100px;" data-hook="ok">Ok</button>
		chromedp.Click(`button[data-hook="ok"]`, chromedp.NodeVisible),

		chromedp.WaitVisible(`button[data-hook="weaps"]`),
		//chromedp.Click(`body > .view-container button.big:nth-child(2)`, chromedp.BySearch),
		chromedp.Evaluate(api, &res),
		chromedp.Evaluate(`window.currentPlayer.join()`, &res),
		//		chromedp.Evaluate(`window.PLAYER.Uh(new KeyboardEvent("keydown", {
		// "key": "d",
		// "keyCode": 68,
		// "which": 68,
		// "code": "KeyD",
		// "location": 0,
		// "altKey": false,
		// "ctrlKey": false,
		// "metaKey": false,
		// "shiftKey": false,
		// "repeat": false
		//}))`, &res),
		chromedp.Evaluate(strings.Replace(getBotByName(worm.botscript), "%%VERBOSITY%%", worm.verbosity, 1), &res),
		//chromedp.Evaluate(`setInterval(() => { element.dispatchEvent(new KeyboardEvent('keydown', {'key': 'd'})); }, 300)`, &res),

		waitforquit(),
		//chromedp.Tasks{
		//	joinandwait(),
		//	chromedp.Click(`body > .view-container button.big:nth-child(2)`, chromedp.BySearch),
		//
		//},

		//chromedp.SendKeys(`canvas`, "d"),
		//chromedp.WaitVisible(`john`),
	)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Go's time.After example:\n%s", example)
}

func waitforquit() chromedp.ActionFunc {
	return func(ctx context.Context) error {
		txt := ""
		err := chromedp.Run(ctx,
			chromedp.WaitVisible(`.chatbox-view .log div.new`),
			chromedp.TextContent(`.chatbox-view .log div.new`, &txt),
		)
		if err != nil {
			fmt.Println("err ----------------" + err.Error())
			return nil
		}
		if strings.Contains(txt, ": !goaway") {
			fmt.Println("----------------quitting", txt)
			var res []byte
			err := chromedp.Run(ctx,
				chromedp.Evaluate(`window.SendChat("ok! cya");`, &res),
				chromedp.Evaluate(`window.PLAYERSESS.Ut();setTimeout(()=>{document.querySelector('.dialog button[data-hook="leave"]').click()}, 1000);`, &res),
				chromedp.WaitVisible(`.roomlist-view`),
			)
			if err != nil {
				fmt.Println("err goaway ----------------" + err.Error())
				return nil
			}
			return nil
		}

		return chromedp.Run(ctx, waitforquit())
	}
}

func replaceScript(ctx context.Context) {
	chromedp.ListenTarget(ctx, func(event interface{}) {
		switch ev := event.(type) {
		case *fetch.EventRequestPaused:
			go func() {
				c := chromedp.FromContext(ctx)
				ctx = cdp.WithExecutor(ctx, c.Target)

				if !strings.Contains(ev.Request.URL, "client.js") {
					//	fmt.Printf("-------[fetch.EventRequestPaused]: %s\n", ev.Request.URL)
					chromedp.Run(ctx, fetch.ContinueRequest(ev.RequestID))
					return
				}
				fmt.Printf("-------------------[fetch.EventRequestPaused]: %s\n", ev.Request.URL)
				status := 200

				headers := make([]*fetch.HeaderEntry, 0)
				headers = append(headers, &fetch.HeaderEntry{Name: "Connection", Value: "closed"})
				headers = append(headers, &fetch.HeaderEntry{Name: "Content-Length", Value: string(len(clientbody))})
				headers = append(headers, &fetch.HeaderEntry{Name: "Content-Type", Value: "application/javascript"})
				params := fetch.FulfillRequest(ev.RequestID, int64(status)).
					WithBody(base64.StdEncoding.EncodeToString([]byte(clientbody))).
					WithResponseHeaders(headers).
					WithResponsePhrase("OK")
				err := params.Do(ctx)
				if err != nil {
					fmt.Println("error " + err.Error())
				}
				fmt.Println("client rewritten")
			}()
		default:
			return
		}
	})
}

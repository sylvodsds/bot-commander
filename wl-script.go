package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const clientScript = "https://ext-proxy.fly.dev/static/client.js"

var clientbody string

func initWLScript() {
	stDir := os.DirFS("./scripts")

	_, err := stDir.Open("client.js")
	if err != nil {
		fmt.Println("client js not found locally")
		downloadAndPatchClientJs()
	}
	f, err := stDir.Open("client.js")

	sbody, err := io.ReadAll(f)
	if err != nil {
		fmt.Println("error reading client js")
	}
	clientbody = string(sbody)
	fa, err := stDir.Open("api.js")

	abody, err := io.ReadAll(fa)
	if err != nil {
		fmt.Println("error reading client js")
	}
	api = string(abody)
}

func downloadAndPatchClientJs() {
	r, err := http.Get(clientScript)
	if err != nil {
		log.Panicf("error retrieving client js %v", err)
	}
	bb, err := io.ReadAll(r.Body)
	if err != nil {
		log.Panicf("error reading client js %v", err)
	}
	bs := string(bb)
	obs := bs
	bs = strings.Replace(bs, `, p = t.K.nl.P();`, `, p = t.K.nl.P();
                    window.ROOMOBJECT = m;
`, 1)
	if obs == bs {
		log.Panicf("error initializing ROOMOBJECT")
	}
	obs = bs
	bs = strings.Replace(bs, `this.w = new za(a.sb,a.W.F.s);`, `            this.w = new za(a.sb,a.W.F.s);
            window.PLAYERSESS = this.w;
            window.SendChat = a.W.sendChat;
`, 1)
	if obs == bs {
		log.Panicf("error initializing PLAYERSESS")
	}
	obs = bs
	bs = strings.Replace(bs, `H.A(this.$t, null)`, `H.A(this.$t, null); if (window.onGameStart) { window.onGameStart(this); }`, 1)
	if obs == bs {
		log.Panicf("error initializing onGameStart")
	}
	obs = bs
	bs = strings.Replace(bs, `null != b && ec.A(a.Fs, b, this.Cd)`, `null != b && ec.A(a.Fs, b, this.Cd);
if (window.onChatMessage) { window.onChatMessage(this.Cd, b.U); }
`, 1)
	if obs == bs {
		log.Panicf("error initializing onChatMessage")
	}
	err = os.WriteFile("./scripts/client.js", []byte(bs), 0666)
	if err != nil {
		log.Panicf("error writing client js %v", err)
	}
}

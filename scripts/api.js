WLPI = 3.141592653589793;
dirRight = 0;
dirLeft = WLPI;
changeWeaponDown = false;

window.currentPlayer = {
    shoot: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyD"}))
    },
    stopShoot: () => {
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyD"}))
    },
    shootOnce: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyD"}))
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyD"}))
    },
    up: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "ArrowUp"}))
    },
    left: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "ArrowLeft"}))
    },
    right: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "ArrowRight"}))
    },
    down: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "ArrowDown"}))
    },
    stopUp: () => {
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "ArrowUp"}))
    },
    stopLeft: () => {
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "ArrowLeft"}))
    },
    stopRight: () => {
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "ArrowRight"}))
    },
    stopDown: () => {
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "ArrowDown"}))
    },
    jump: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyS"}))
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyS"}));
    },
    rope: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyA"}))
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyS"}))
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyS"}));
    },
    stopRope: () => {
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyA"}))
    },
    cutRope: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyS"}))
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyS"}))
    },
    weaponUp: () => {
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "KeyA"}))
        window.ROOMOBJECT.Uh(new KeyboardEvent("keydown", {"code": "ArrowLeft"}))
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "ArrowLeft"}))
        window.ROOMOBJECT.Vh(new KeyboardEvent("keyup", {"code": "KeyA"}))
    },
    getPosition: () => {
        let cp = window.currentPlayer.getPlayerObject();
        return { x: cp.qa.x, y: cp.qa.y, aimAngle: cp.qa.Oa, direction: cp.qa.direction }
    },
    getMyName: () => {
        let cp = window.currentPlayer.getPlayerObject();
        return cp.L
    },
    getPlayerObject : () => { //  B= Map contains  class Ka
       return window.ROOMOBJECT.Tb.W.B.get(window.ROOMOBJECT.Tb.sb);
    },
    getPlayersList : () => {
        return  window.ROOMOBJECT.Tb.W.B;
    },
    getPlayersNamesAndColor: () => {
        let l = []
        for (let  [id, pl]  of window.currentPlayer.getPlayersList()) {
            l.push({
                name: pl.L,
                red :(pl.pc&0xFF0000) >> 16 ,
                green : (pl.pc&0xFF00)  >> 8,
                blue : (pl.pc&0xFF)
            })
        }
    },
    getOtherOnGamePlayersList : () => {
        let all =  window.currentPlayer.getPlayersList();
        let apl = new Map();
        for (let [id, pl] of all) {
            if (id!= window.ROOMOBJECT.Tb.sb && pl.qa && pl.qa.team!=0) {
                pl.position = { x: pl.qa.x, y: pl.qa.y, aimAngle: pl.qa.Oa, direction: pl.qa.direction }
                pl.name = pl.L
                apl.set(id, pl)
            }
        }
        return apl;
    },
    getMap: () => { // F = class Ma / level == class Z
        return window.ROOMOBJECT.Tb.W.F.level;
        // let l =  window.ROOMOBJECT.Tb.W.F.level;
        // return {
        //     height: l.height,
        //     width: l.width,
        //     name: l.name,
        //     data: l.data
        // }
    },
    getCurrentWeapon: () => {
        let cp = window.currentPlayer.getPlayerObject();
        let cwidx = cp.zf;
        let currentWeaponState = cp.Pb[cwidx];
        if (currentWeaponState.id<0)  return null;

        let weaponDef = window.ROOMOBJECT.Tb.W.F.s.O[currentWeaponState.id];
        if (typeof weaponDef =='undefined') return null;

        return {
            reload: currentWeaponState.hb?Math.round((currentWeaponState.hb/(weaponDef.Ni* window.ROOMOBJECT.Tb.W.F.je))*100):0,
            ammo: currentWeaponState.ga,
            maxAmmo: weaponDef.ga,
            name: weaponDef.name,
          //  shotType: weaponDef. from wobject
        }
    },
    getMod: () => { // s = class ra contains mod // breakpoint on `null != p && 5 == p.length ? f.￼￼sa(Aa.￼￼Y(p)) : f.￼￼sa(Aa.￼￼Y([250, 250, 250, 250, 250]));
        let m = window.ROOMOBJECT.Tb.W.F.s;
        return {
            materials: m.Ba,
            palette: m.Fa,
            filename: m.Yi,
            name: m.name,
            author: m.xi,
            weapons: m.O, // Array of class cc
            minAngle: m.pb.lh,
            maxAngle: m.pb.kh,
        }
    },
    setOnChatMessage: (callback) => {
        window.onChatMessage = callback;
    },
    setOnGameStart: (callback) => {
        window.onGameStart = callback;
    },
    sendChat: window.SendChat,
    setOnTick: (callback) => {
        let cnt = 0;
        window.hack = (st) => {
            cnt++
            return callback(st, cnt)
        }
    },
    join: () => {
        console.log("join called");
        if (document.querySelector('button[data-hook="join-t1"]')) {
            document.querySelector(`button[data-hook^="join-t"]:not([disabled])`).click()
        } else {
            document.querySelector('button[data-hook="join"]').click()
        }
    }
}

console.log("api loaded");
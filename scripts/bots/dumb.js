let R_VERBOSITY = /\s*(verbosity|v)=(normal|n|error|e|debug|d)\s*/i;
let R_HELP = /\s*(help|h)\s*/i;
let R_GOAWAY = /\s*(goaway|h)\s*/i;
const log= (txt) => {
    api.sendChat(txt);
    console.log(txt);
}
const verbosityReplaced = '%%VERBOSITY%%'
let CONFIG = {
    verbosity: verbosityReplaced==''?'normal':verbosityReplaced
}
let VERBOSE= {
    isDebug: () => ['error','debug'].includes(CONFIG.verbosity),
    isError: () => ['error'].includes(CONFIG.verbosity),
}
let api = window.currentPlayer;

let WLMap = null;
let WLMod = null;
let Me = null;
let maxAngle = null;
let minAngle = null;

let target = null;
let angle = 0;

let initBot = () => {
    Me = api.getPlayerObject();
    WLMap = api.getMap();
    WLMod = api.getMod();
    maxAngle = WLMod.maxAngle;
    minAngle = WLMod.minAngle;
    if (VERBOSE.isDebug()) {
        log(`map: ${WLMap.name} ${WLMap.width}x${WLMap.height}`)
        log(`mod: ${WLMod.name} ${WLMod.filename} ${WLMod.author} angle ${maxAngle} ${minAngle}`)
    }
}

api.setOnGameStart(initBot)
initBot()

api.sendChat(`hi it's me ${api.getMyName()} I'm a dumb bot I'm number ${Me.U} verbosity=${CONFIG.verbosity}`);
api.setOnChatMessage((msg/*, owner*/) => {
    //if (owner==Me.U) return
    if (msg.startsWith(`@${api.getMyName().replaceAll(' ','_')}`)) {
        api.sendChat(`hi i'm a bot`)
        if (R_VERBOSITY.test(msg)) {
            let setverb = R_VERBOSITY.exec(msg)[2].toLowerCase();
            CONFIG.verbosity = setverb
            api.sendChat(`verbosity set to ${setverb}`);
        }
        if (R_GOAWAY.test(msg)) {
            api.sendChat(`to be implemented, type "!goaway" instead to get rid of all bots`);
        }
        if (R_HELP.test(msg)) {
            api.sendChat(`you can ping me using "@${api.getMyName()}" followed by:`);
            api.sendChat(`- help (or h): to print this help message`);
            api.sendChat(`- verbosity=verb (or v=verb):`);
            api.sendChat(`     to change verbosity where verb can be (normal|n|error|e|debug|d)`);
            api.sendChat(`- goaway: so I leave the room`);
        }
    }
})

let getTarget = () => {
    let pl = api.getOtherOnGamePlayersList();
    let position = api.getPosition();

    let d = 0;
    if (!target || target.qa==null) {
        for (let [id, p] of pl) {
            let dist = (p.position.x - position.x) * (p.position.x - position.x) + (p.position.y - position.y) * (p.position.y - position.y);
            if(!d || dist<d){ // closest target
                d = dist;
                target = p;
            }
        }
    }
    if (VERBOSE.isDebug()) {
        log((target?` target: ${target.name}`:'')+' from '+position.x+' '+position.y+' to '+target.qa.x+' '+target.qa.y);
    }
}

let anglechangeInt =null

api.setOnTick((st, cnt)=>{
    try {
        aimAndshoot(cnt)
    }catch (e) {
        if (VERBOSE.isDebug()) {
            if (cnt%200==0) api.sendChat("error aiming"+e.message)
        }qs
    }

    if (cnt%200!=0) return;
    let position = null
    let cp = api.getPlayerObject();

    try {
        let cw = api.getCurrentWeapon();
        if (cw == null) {
            return
        }
        if (!cw.ammo) {
            api.stopShoot()
        }
        position = api.getPosition()
        if (VERBOSE.isDebug()) {
            log(`x ${position.x} y ${position.y} aimAngle ${position.aimAngle} direction ${position.direction}`);
        }
        getTarget()

    } catch (e) {
        if (VERBOSE.isDebug()) {
            log("out of game"+e.message)
            target = null;
        }
        return
    }

    /*
    if (Math.random() < 0.1) {
        window.currentPlayer.jump();
    }
    if ( Math.random() < 0.5) {
        window.currentPlayer.stopRight();
        window.currentPlayer.left();
    } else if ( Math.random() > 0.8) { window.currentPlayer.weaponUp() } else { window.currentPlayer.stopLeft();window.currentPlayer.right();}
    */

});

function aimAndshoot(cnt) {
    if (target) {
        if (target.qa==null) {
            target=null;
            api.stopShoot();
            return;
        }
        let cp = api.getPlayerObject();
        let position = api.getPosition();
        let toX = Math.round(target.qa.x)
        let toY = Math.round(target.qa.y)
        let meX = Math.round(position.x)
        let meY = Math.round(position.y)
        let dir = (meX-toX)>0?0:1;
        let ydiff = meY-toY;
        let xdiff = Math.abs(meX-toX);
        let angle = xdiff?  Math.atan2(ydiff,  xdiff):0;
        if (dir!=position.direction) {
            if (dir) {
                api.stopRope()
                api.stopLeft()
                api.right()
                setTimeout(api.stopRight,150)

            } else {
                api.stopRope()
                api.stopRight()
                api.left()
                setTimeout(api.stopLeft,150)
            }

        }

        /*
          while(cp.qa.Oa.toPrecision(4)!=angle.toPrecision(4)) {
                  if ([minAngle,maxAngle].includes(cp.qa.Oa)) {
                      vertDir = null
                      break;
                  }
          }
          */
        if (VERBOSE.isDebug()) {
            if (cnt%200==0)    api.sendChat(cp.qa.Oa.toPrecision(1)+' '+angle.toPrecision(1)+' '+cp.qa.Oa+' '+angle);
        }
        if (cp.qa.Oa.toPrecision(1)==angle.toPrecision(1)) {
        //    api.sendChat(`aim looks ok ${angle.toPrecision(4)}`);
            api.stopDown();api.stopUp()
            shoot()
        } else {
            api.stopDown();api.stopUp()
            if (cp.qa.Oa.toPrecision(1)>angle.toPrecision(1)) {
                if (cp.qa.Oa.toPrecision(1)==minAngle.toPrecision(1)) {
                    shoot()
                } else {
                    if (cnt%2==0)   api.down()
                }

            } else {
                if (cp.qa.Oa.toPrecision(1)==maxAngle.toPrecision(1)) {
                    shoot()
                } else {
                    if (cnt%2==0)  api.up()
                }
            }
        }

    }
}

function shoot() {
    let cw = api.getCurrentWeapon();
    if (cw == null) {
        return
    }
    if (!cw.ammo) {
        api.weaponUp();
    }
    api.shoot();
}
/*
setInterval(()=>{
    let c =0;
    let pos = window.currentPlayer.getPosition();
    let direction = ( pos.y > 250 || Math.random() < 0.5) ? "Up":"Down";
    window.currentPlayer[direction.toLowerCase()]();

    window.hack = (a, b) => {
        c++
        if (c>= 10) {
            window.hack = null;
            window.currentPlayer['stop'+direction]();
            let startRope = () => {
                if ( pos.y > 200 || Math.random() < 0.3) {
                    window.currentPlayer.rope()
                    window.currentPlayer.stopRope()
                    let cutrope = () => {
                        if ( Math.random() < 0.3) {
                            window.currentPlayer.cutRope()
                        } else {
                            setTimeout(cutrope,4000)
                        }
                    }
                    setTimeout(cutrope,4000)
                }
            }
            startRope()
        }
    }

},10000);
*/